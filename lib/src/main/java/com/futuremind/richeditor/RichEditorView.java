package com.futuremind.richeditor;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.text.Editable;
import android.text.Html;
import android.text.Selection;
import android.text.Spannable;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.CharacterStyle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.futuremind.richeditor.effects.BoldEffect;
import com.futuremind.richeditor.effects.ItalicEffect;
import com.futuremind.richeditor.effects.UnderlineEffect;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by pawelcala on 28/08/15.
 */
public class RichEditorView extends EditText {

    public static final String EFFECT_BOLD = "strong";
    public static final String EFFECT_ITALIC = "italic";
    public static final String EFFECT_UNDERLINE = "underline";

    private boolean mIsBackspace;
    private int mLastPosition;
    private int mStyleStart;

    /**
     * Prevents onSelectionChange update
     */
    private boolean textChanged = false;

    private List<Class<?>> availableSpanClasses = new LinkedList<>();
    private Map<String, Class<? extends CharacterStyle>> mappedEffects = new HashMap<>();
    private Map<Class<? extends CharacterStyle>, Boolean> onFlags = new HashMap<>();
    private Map<String, View> effectsWatchers = new HashMap<>();
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            mIsBackspace = after < count;
            textChanged = true;
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (mIsBackspace) removedUnusedSpans(s);
            int position = Selection.getSelectionStart(getText());
            if ((mIsBackspace && position != 1) || mLastPosition == position)
                return;

            debugSpans(s);

            if (position < 0) {
                position = 0;
            }
            mLastPosition = position;
            if (position > 0) {
                if (mStyleStart > position) {
                    mStyleStart = position - 1;
                }

                Object[] allSpans = s.getSpans(mStyleStart, position, Object.class);
                allSpans = filterActiveSpans(allSpans);
                for (Object span : allSpans) {
                    int spanStart = s.getSpanStart(span);
                    int spanEnd = s.getSpanEnd(span);
                    int spanFlag = s.getSpanFlags(span);

                    if (spanEnd >= mStyleStart && (spanFlag & Spannable.SPAN_EXCLUSIVE_EXCLUSIVE) == Spannable.SPAN_EXCLUSIVE_EXCLUSIVE) {
                        s.removeSpan(span);
                        s.setSpan(getSpanObject(getEffectByClass(span.getClass())), spanStart, position, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                    }


                    for (Map.Entry<Class<? extends CharacterStyle>, Boolean> entry : onFlags.entrySet()) {
                        Class clazz = entry.getKey();
                        if (span.getClass().equals(clazz)) {
                            entry.setValue(false);
                            break;
                        }
                    }
                }


                for (Map.Entry<Class<? extends CharacterStyle>, Boolean> entry : onFlags.entrySet()) {
                    if (entry.getValue())
                        s.setSpan(newEffectInstance(getEffectByClass(entry.getKey())), mStyleStart, position, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                }
            }

            notifyEffectWatchers();
        }
    };

    private void removedUnusedSpans(Editable s) {
        Object[] spans = s.getSpans(0, s.length(), Object.class);
        spans = filterActiveSpans(spans);
        for (Object span : spans) {
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            int flag = s.getSpanFlags(span);
            if (end - start == 0) {
                s.removeSpan(span);
            }
        }
    }


    public RichEditorView(Context context) {
        super(context);
        init(context, null);
    }

    public RichEditorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public RichEditorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public RichEditorView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        addTextChangedListener(textWatcher);
        initWithDefaultEffects();
    }

    public void initWithDefaultEffects() {
        addEffect(EFFECT_BOLD, BoldEffect.class);
        addEffect(EFFECT_ITALIC, ItalicEffect.class);
        addEffect(EFFECT_UNDERLINE, UnderlineEffect.class);
    }

    public void addEffect(String effectTag, Class<? extends CharacterStyle> effectClass) {
        mappedEffects.put(effectTag, effectClass);
        availableSpanClasses.add(effectClass);
        onFlags.put(effectClass, false);
    }

    private CharacterStyle newEffectInstance(String effectTag) {
        try {
            return (CharacterStyle) getClassByEffect(effectTag).newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
            return null;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onSelectionChanged(int selStart, int selEnd) {
        if (!textChanged) {
            notifyEffectWatchers();
        }
        textChanged = false;
        super.onSelectionChanged(selStart, selEnd);
    }

    private void notifyEffectWatchers() {
        int selectionStart = getSelectionStart();
        int selectionEnd = getSelectionEnd();
        boolean textSelection = selectionEnd - selectionStart > 0;

        if(  onFlags == null ) return;
        for (Map.Entry<Class<? extends CharacterStyle>, Boolean> entry : onFlags.entrySet()) {
            entry.setValue(false);
        }


        Object[] activeSpans = getText().getSpans(selectionStart, selectionEnd, Object.class);
        activeSpans = filterActiveSpans(activeSpans);


        for (Object span : activeSpans) {
            int spanStart = getText().getSpanStart(span);
            int spanEnd = getText().getSpanEnd(span);
            int spanFlag = getText().getSpanFlags(span);

            if (!textSelection
                    && ((spanStart == selectionStart)
                    //|| (spanEnd == selectionStart && (spanFlag & Spannable.SPAN_EXCLUSIVE_EXCLUSIVE) == Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            ))
                continue;
            if (textSelection && !allSelected(span, selectionStart, selectionEnd)) continue;


            for (Map.Entry<Class<? extends CharacterStyle>, Boolean> entry : onFlags.entrySet()) {
                Class clazz = entry.getKey();
                if (span.getClass().equals(clazz)) {
                    entry.setValue(true);
                    break;
                }
            }
        }

        invalidateWatchersState();
    }


    private Object[] filterActiveSpans(Object[] spans) {
        ArrayList filteredSpans = new ArrayList();
        for (Object span : spans) {
            if (availableSpanClasses != null && availableSpanClasses.contains(span.getClass())) {
                filteredSpans.add(span);
            }
        }

        return filteredSpans.toArray(new Object[filteredSpans.size()]);
    }

    private boolean toggleEffectInSelection(String effect, int startSelection, int endSelection) {
        Editable editable = getText();
        Object[] spans = editable.getSpans(startSelection, endSelection, getClassByEffect(effect));
        adjustSpansToSelection(editable, spans, effect, startSelection, endSelection);
        spans = editable.getSpans(startSelection, endSelection, getClassByEffect(effect));
        mergeSelectionSpans(spans, effect, startSelection, endSelection);
        spans = editable.getSpans(startSelection, endSelection, getClassByEffect(effect));
        if (spans.length == 1 && allSelected(spans[0], startSelection, endSelection)) {
            editable.removeSpan(spans[0]);
            return false;
        } else {
            spans = editable.getSpans(startSelection, endSelection, getClassByEffect(effect));
            for (Object span : spans) {
                editable.removeSpan(span);
            }
            editable.setSpan(getSpanObject(effect), startSelection, endSelection, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            return true;
        }
    }

    private void mergeSelectionSpans(Object[] spans, String effect, int startSelection, int endSelection) {
        int width = endSelection - startSelection;
        int spansWidth = 0;
        for (Object span : spans) {
            int spanStart = getText().getSpanStart(span);
            int spanEnd = getText().getSpanEnd(span);
            int spanWidth = spanEnd - spanStart;
            spansWidth += spanWidth;
        }

        if (spansWidth == width) {
            for (Object span : spans) {
                getText().removeSpan(span);
            }

            getText().setSpan(getSpanObject(effect), startSelection, endSelection, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        }
    }

    private boolean allSelected(Object what, int startSelection, int endSelection) {
        int spanStart = getText().getSpanStart(what);
        int spanEnd = getText().getSpanEnd(what);
        return spanStart <= startSelection && spanEnd >= endSelection;
    }

    private void adjustSpansToSelection(Spannable s, Object[] spans, String effect, int startSelection, int endSelection) {
        for (Object span : spans) {
            int spanStart = s.getSpanStart(span);
            int spandEnd = s.getSpanEnd(span);

            if (spanStart < startSelection && endSelection < spandEnd) {
                splitSpan(s, span, effect, spanStart, spandEnd, startSelection, endSelection);
            } else if (spanStart < startSelection) {
                splitSpan(s, span, effect, spanStart, spandEnd, startSelection);
            } else if (endSelection < spandEnd) {
                splitSpan(s, span, effect, spanStart, spandEnd, endSelection);
            }
        }
    }

    private void splitSpan(Spannable s, Object span, String effect, int spanStart, int spanEnd, int splitStartPosition, int splitEndPosition) {
        s.removeSpan(span);
        s.setSpan(getSpanObject(effect), spanStart, splitStartPosition, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(getSpanObject(effect), splitStartPosition, splitEndPosition, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(getSpanObject(effect), splitEndPosition, spanEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    private void splitSpan(Spannable s, Object span, String effect, int spanStart, int spandEnd, int splitPosition) {
        s.removeSpan(span);
        s.setSpan(getSpanObject(effect), spanStart, splitPosition, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        s.setSpan(getSpanObject(effect), splitPosition, spandEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    private Class<?> getClassByEffect(String effectTag) {
        return mappedEffects.get(effectTag);
    }

    private String getEffectByClass(Class<?> clazz) {

        if (mappedEffects.containsValue(clazz)) {
            for (Map.Entry<String, Class<? extends CharacterStyle>> entry : mappedEffects.entrySet()) {
                if (clazz.equals(entry.getValue())) return entry.getKey();
            }
        }

        return null;
    }

    private boolean spanInSelectionBounds(Object span, int selectionStart, int selectionEnd) {
        int spanStart = getText().getSpanStart(span);
        int spanEnd = getText().getSpanEnd(span);
        int flags = getText().getSpanFlags(span);

        return true;
    }

    private void invalidateWatchersState() {
        if (effectsWatchers == null) return;
        Set<String> keyes = effectsWatchers.keySet();
        for (String key : keyes) {
            boolean isOn = onFlags.get(getClassByEffect(key));
            notifyWatcher(key, isOn);
        }
    }

    private void notifyWatcher(String key, boolean active) {
        Log.d("RichEditorView", "notifyWatcher() called with: " + "key = [" + key + "], active = [" + active + "]");
        View v = effectsWatchers.get(key);
        if (v != null) v.setSelected(active);
    }

    public void addEffectWatcher(String effect, View v) {
        effectsWatchers.put(effect, v);
    }

    public void setHtml(String html) {
        if (html == null) {
            setText("");
        } else {
            setText(Html.fromHtml(html));
        }
    }

    public String toHtml() {
        String html = Html.toHtml(getText());
        String paragraphRegex = "<p[ a-zA-Z=\"]+>";
        html = html.replaceAll(paragraphRegex, "<p>"); //<feels_good_meme.jpg>
        return html;
    }

    public boolean toggleEffect(String tag) {
        Spannable s = getText();
        if (s == null)
            return false;

        debugSpans(s);

        boolean shouldSplitSpan = false;
        int selectionStart = getSelectionStart();
        int selectionEnd = getSelectionEnd();
        int splitPosition = 0;
        mStyleStart = selectionStart;

        if (selectionStart > selectionEnd) {
            int temp = selectionEnd;
            selectionEnd = selectionStart;
            selectionStart = temp;
        }

        Class styleClass = getClassByEffect(tag);
        if (styleClass == null) return false;

        boolean isOn = !onFlags.get(styleClass);
        onFlags.put(styleClass, isOn);
        boolean isChecked = isOn;

        boolean textIsSelected = selectionEnd > selectionStart;
        if (textIsSelected) {
            isChecked = toggleEffectInSelection(tag, selectionStart, selectionEnd);
        } else {
            Object[] allSpans = s.getSpans(selectionStart, selectionEnd, styleClass);

            boolean shouldAddSpan = true;
            for (Object span : allSpans) {
                if (!isChecked && textIsSelected) {
                    // If span exists and text is selected, remove the span
                    s.removeSpan(span);
                    shouldAddSpan = false;
                    break;
                } else if (!isChecked) {
                    // Remove span at cursor point if button isn't checked
                    Object[] spans = s.getSpans(mStyleStart - 1, mStyleStart, styleClass);
                    for (Object removeSpan : spans) {

                        if (selectionStart - selectionEnd == 0) {
                            shouldSplitSpan = true;
                            splitPosition = selectionStart;
                        }
                        selectionStart = s.getSpanStart(removeSpan);
                        selectionEnd = s.getSpanEnd(removeSpan);
                        s.removeSpan(removeSpan);
                    }
                }
            }

            if (shouldAddSpan) {
                if (shouldSplitSpan) {
                    s.setSpan(getSpanObject(tag), selectionStart, splitPosition, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    s.setSpan(getSpanObject(tag), splitPosition, selectionEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                } else {
                    s.setSpan(getSpanObject(tag), selectionStart, selectionEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
        }

        notifyEffectWatchers();
        //debugSpans(s);
        return isChecked;
    }

    private void debugSpans(Spannable s) {
        Log.d("Spans", "debugSpans");
        Object[] objs = s.getSpans(0, s.length(), Object.class);
        objs = filterActiveSpans(objs);
        for (Object object : objs) {
            Log.d("Spans", String.format("Span: %s, position:%d-%d, flags:%d", object.getClass().getName(), s.getSpanStart(object), s.getSpanEnd(object), s.getSpanFlags(object)));
        }
    }

    private Object getSpanObject(String tag) {
        Class styleClass = getClassByEffect(tag);
        Object span = null;
        try {
            span = styleClass.newInstance();
            return span;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }


}
