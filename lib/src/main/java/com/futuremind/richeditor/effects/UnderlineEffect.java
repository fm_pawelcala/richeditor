package com.futuremind.richeditor.effects;

import android.os.Parcel;
import android.text.style.UnderlineSpan;

/**
 * Created by pawelcala on 10/14/15.
 */
public class UnderlineEffect extends UnderlineSpan {

    public UnderlineEffect() {
    }

    public UnderlineEffect(Parcel src) {
        super(src);
    }
}

