package com.futuremind.richeditor.effects;

import android.graphics.Typeface;
import android.os.Parcel;
import android.text.style.StyleSpan;

/**
 * Created by pawelcala on 10/14/15.
 */
public class ItalicEffect extends StyleSpan {

    public ItalicEffect() {
        super(Typeface.ITALIC);
    }

    public ItalicEffect(Parcel src) {
        super(src);
    }
}