package com.futuremind.richeditor.effects;

import android.graphics.Typeface;
import android.os.Parcel;
import android.text.style.StyleSpan;

/**
 * Created by pawelcala on 10/14/15.
 */
public class BoldEffect extends StyleSpan {

    public BoldEffect() {
        super(Typeface.BOLD);
    }

    public BoldEffect(Parcel src) {
        super(src);
    }
}
