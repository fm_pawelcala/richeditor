package com.futuremind.richeditor.sample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.futuremind.richeditor.RichEditorView;

public class MainActivity extends AppCompatActivity {

    private RichEditorView richEditorView;
    private View richEffectB;
    private View richEffectI;
    private View richEffectU;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        richEffectB = findViewById(R.id.rich_effect_b);
        richEffectI = findViewById(R.id.rich_effect_i);
        richEffectU = findViewById(R.id.rich_effect_u);
        richEditorView = (RichEditorView) findViewById(R.id.richeditorview);

        richEditorView.addEffectWatcher(RichEditorView.EFFECT_BOLD, richEffectB);
        richEditorView.addEffectWatcher(RichEditorView.EFFECT_ITALIC, richEffectI);
        richEditorView.addEffectWatcher(RichEditorView.EFFECT_UNDERLINE, richEffectU);

        richEffectB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                richEditorView.toggleEffect(RichEditorView.EFFECT_BOLD);
            }
        });

        richEffectI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                richEditorView.toggleEffect(RichEditorView.EFFECT_ITALIC);
            }
        });

        richEffectU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                richEditorView.toggleEffect(RichEditorView.EFFECT_UNDERLINE);
            }
        });
    }
}
